<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Open a given resource in Amanote.
 *
 * If the user doesn't have access to the resource, it will search if there is another
 * corresponding resource in an other course (from backup/restore, for instance) in
 * which the current user is enrolled in.
 *
 * @package     mod_amanote
 * @copyright   2021 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../config.php');
require_once('../../lib/externallib.php');

// Verify that the current user is logged in.
require_login();

// Initialize params.
$amaresourceid = isset($_GET['amaResourceId']) ? $_GET['amaResourceId'] : null;
$pagenumber    = isset($_GET['pageNumber']) ? $_GET['pageNumber'] : '1';

if (!$amaresourceid || !preg_match('/\d+\.\d+\.\d+/', $amaresourceid)) {
    echo 'Invalid resource id.';
    exit();
}

$explodedid = explode('.', $amaresourceid);
$courseid   = $explodedid[0];
$modid      = $explodedid[1];
$fileid     = $explodedid[2];

// Check if the link was clicked from a specific course in Moodle.
$referercourseid = null;

if (isset($_SERVER['HTTP_REFERER'])) {
    // Parse the referer URL.
    $url = parse_url($_SERVER['HTTP_REFERER']);
    $query = array();
    parse_str(html_entity_decode($url['query']), $query);

    // Check if the referer URL matches to a course module.
    if ($url['host'] == $_SERVER['SERVER_NAME'] && array_key_exists('cmid', $query)) {
        $result = $DB->get_record_sql("SELECT course FROM mdl_course_modules WHERE id=:cmid", array('cmid' => $query['cmid']));
        $referercourseid = $result->course;
    }
}

// Get the file corresponding to the given id in a course in which the user is enrolled.
$sql = "SELECT
          mdl_files.id AS id,
          mdl_files.contextid,
          mdl_files.component,
          mdl_files.filearea,
          mdl_files.filename,
          mdl_course_modules.instance AS modid,
          mdl_course_modules.course AS courseid
        FROM mdl_files
        INNER JOIN mdl_context ON mdl_context.id = mdl_files.contextid
        INNER JOIN mdl_course_modules ON mdl_course_modules.id = mdl_context.instanceid
        INNER JOIN mdl_course on mdl_course.id = mdl_course_modules.course
        WHERE mdl_files.component='mod_amanote'
        AND (mdl_files.contenthash, mdl_files.timecreated)=
            (SELECT contenthash,timecreated FROM mdl_files
            WHERE mdl_files.id=:fileid)
          AND mdl_course_modules.course IN
            (SELECT courseId FROM mdl_enrol
            INNER JOIN mdl_user_enrolments ON mdl_enrol.id = mdl_user_enrolments.enrolid
            WHERE mdl_user_enrolments.userid=:userid)";

$params = array('fileid' => $fileid, 'userid' => $USER->id);

if ($referercourseid != null) {
    $sql .= " AND mdl_course_modules.course=:referercourseid";
    $params['referercourseid'] = $referercourseid;
    $result = $DB->get_records_sql($sql, $params);
} else {
    $sql .= " ORDER BY mdl_course.timecreated ASC";
    $result = $DB->get_records_sql($sql, $params);
}

if (!$result || count($result) <= 0) {
    echo 'You don\'t have access to this resource.';
    exit();
}

$files = array_values($result);
$file  = array_shift($files);

// Generate the Amanote's URL.
$amaresourceid = $file->courseid . '.' . $file->modid . '.' . $file->id;

$serviceparams = array('shortname' => MOODLE_OFFICIAL_MOBILE_SERVICE, 'enabled' => 1);
$service       = $DB->get_record('external_services', $serviceparams);

if (empty($service)) {
    echo 'Amanote service not found.';
    exit();
}

$config          = get_config('mod_amanote');
$siteurl         = $CFG->wwwroot;
$language        = substr($USER->lang, 0, 2);
$usercontext     = context_user::instance($USER->id);
$privatefilepath = '/' . $usercontext->id . '/user/private/Amanote/' . $file->contextid . '.ama';
$moodleversion   = preg_replace('/(\d+\.\d+(\.\d+)?) .*$/', '$1', $CFG->release);
$token           = external_generate_token_for_current_user($service);
$pdfpath         = "/$file->contextid/$file->component/$file->filearea/1/$file->filename";

$url = 'https://app.amanote.com/' . $language  . '/moodle/note-taking?' .
       'siteURL='. $siteurl . '&' .
       'accessToken=' . $token->token . '&' .
       'tokenExpDate=' . $token->validuntil . '&' .
       'userId=' . $USER->id . '&' .
       'pdfPath=' . $pdfpath . '&' .
       'amaPath=' . $privatefilepath . '&' .
       'resourceId=' .$file->modid . '&' .
       'autosavePeriod=' . $config->autosaveperiod . '&' .
       'saveInProvider=' . $config->saveinprivate . '&' .
       'providerVersion=' . $moodleversion . '&' .
       'pluginVersion=' . $config->version . '&' .
       'fileId=' . $file->id . '&' .
       'courseId=' . $file->courseid . '&' .
       'pageNumber=' . $pagenumber;

// Redirect to Amanote.
echo "If you are not redirected please <a href=\"$url\">click here</a>
<script language='JavaScript'>
window.location.href='$url';
</script>";
